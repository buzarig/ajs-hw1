"use strict";

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  get personName() {
    return this.name;
  }
  get personAge() {
    return this.age;
  }
  get personSalary() {
    return this.salary;
  }

  set personName(value) {
    this.name = value;
  }
  set personAge(value) {
    this.age = value;
  }
  set personSalary(value) {
    this.salary = value;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  get personSalary() {
    return this.salary * 3;
  }
}

let oleg = new Programmer("Oleg", 36, 2000, "ua");
let dima = new Programmer("Dima", 22, 4000, "en");

console.log(oleg, dima);
